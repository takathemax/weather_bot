import {Const} from './settings/const';
import {SlackBot} from './slack/slackbot';
import {Forecaster} from './weather/forecaster';

// Slackへ通知
function notifyToSlack(): void{
    let forecastResult = Forecaster.getResultMessage();
    let bot = new SlackBot(Const.Slack.BOT_NAME, Const.Slack.BOT_ICON);
    bot.talk(forecastResult, Const.Slack.CHANNEL_NAME)   
}