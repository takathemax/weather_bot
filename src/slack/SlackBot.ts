import {SecurePropety} from '../settings/secure';

export class SlackBot{
    app =  SlackApp.create(SecurePropety.SLACK_ACCESS_TOKEN);
    name: string = ''
    icon: string = ''
    constructor(name: string, icon: string){
        this.name = name;
        this.icon = icon;
    }

    talk(message: string, channnel: string): void{
        let option = {
            username: this.name,
            icon_emoji: this.icon
        }; 
        return this.app.postMessage(channnel,
            message, 
            option);
    }
}
