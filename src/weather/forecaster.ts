import {Const} from '../settings/const';

export class Forecaster{
    static getResultMessage(): string{
        let res = getForecast(Const.WeatherForecast.City.TOKYO);
        return prepareWeatherInfomation(res);
    }
}

function getForecast(cityId: string): any{
    let apiUrl: string = Const.WeatherForecast.API_URL + '?city=' + cityId;
    let options: { [key: string]: string; } = {
        'method': 'GET',
        'contentType': 'application/json'
      };
    let response = UrlFetchApp.fetch(apiUrl, options).getContentText();
    return JSON.parse( response )
}

function prepareWeatherInfomation(json): string{
    let title: string =  json.title;

    let todayForecasts = json.forecasts[1];
    let weather: string = todayForecasts.telop;
    let maxTemperature: string = todayForecasts.temperature.max.celsius;
    let minTemperature: string = todayForecasts.temperature.min.celsius;
    let imageUrl: string = todayForecasts.image.url;

    let message: string = '<!channel> \n 明日の天気をおしらせします‼️' + '\n\n' +
                  title + '\n' +
                  '天気 : ' + weather + '\n' +
                  '最高気温 : ' + maxTemperature + '\n' +
                  '最低気温 : ' + minTemperature + '\n' +
                  imageUrl;
    
    return message;
}
