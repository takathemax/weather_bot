export namespace Const.Slack{
    export const BOT_NAME: string = "お天気Bot!!";
    export const BOT_ICON: string = ":satellite_antenna:"; 
    export const CHANNEL_NAME: string = "#weather";
}

export namespace Const.WeatherForecast{
    export const API_URL: string = "http://weather.livedoor.com/forecast/webservice/json/v1";
}

export namespace Const.WeatherForecast.City{
    export const TOKYO: string = "130010";
}
